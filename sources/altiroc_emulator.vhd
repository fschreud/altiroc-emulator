--! This file is part of the altiroc emulator
--! Copyright (C) 2001-2022 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.
library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.vcomponents.all;
library XPM;
use XPM.vcomponents.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;

entity altiroc_emulator is
generic (
    DATARATE : integer := 320;
    packetlength: integer := 16
);
port (
    LPGBT_HARD_RSTB : in std_logic;  -- From lpGBT GPIO
    LPGBT_CLK40M_P  : in std_logic;  -- 40MHz from lpGBT ECLK
    LPGBT_CLK40M_N  : in std_logic;
    FAST_CMD_P      : in std_logic;  -- From Timing lpGBT Elink
    FAST_CMD_N      : in std_logic;
    TIMING_DOUT_P   : out std_logic_vector(1 downto 0);  -- To Timing lpGBT Elink
    TIMING_DOUT_N   : out std_logic_vector(1 downto 0);
    LUMI_DOUT_P     : out std_logic_vector(1 downto 0);  -- To Lumi lpGBT Elink
    LUMI_DOUT_N     : out std_logic_vector(1 downto 0);
    --I2C_ADDR        : in std_logic_vector(3 downto 1);   -- Config by PEB
    --I2C_SCL         : in std_logic;                      -- From Timing lpGBT I2C master
    --I2C_SDA         : inout  std_logic;
    -- Test
    REFCLK_P: in std_logic;        -- Local OSC, 200MHz
    REFCLK_N: in std_logic
    --DIPSW:    in std_logic_vector(2 downto 0);        -- Switch SW1
    --TESTPIN: inout std_logic_vector(1 downto 0);          -- Connector J1
    --TP: out std_logic_vector(2 downto 1)                -- 
);
end entity altiroc_emulator;

architecture rtl of altiroc_emulator is

    signal reset: std_logic;
    signal clk_wiz_reset, locked: std_logic;
    signal dataclk, serialclk: std_logic;
    signal clk160: std_logic;
    signal clk320: std_logic;
    signal clk640: std_logic;
    signal clk32, clk64, clk128, clk40: std_logic;
    signal clk200, clk200_ibuf: std_logic;
    signal data_10b, data_10b_inv: std_logic_vector(9 downto 0);
    signal data_8b: std_logic_vector(7 downto 0);
    signal CharIsK: std_logic;
    --signal tready: std_logic;
    
    constant K_28_5  : std_logic_vector (7 downto 0) := "10111100"; -- K28.5
    constant K_28_7  : std_logic_vector (7 downto 0) := "11111100"; -- K28.7

    signal trigger, bcr, cal, gbrst, settrigid, synclumi, fastcmd_locked: std_logic;
    signal trigid: std_logic_vector(11 downto 0);
    signal L1ID: std_logic_vector(23 downto 0);
    signal XL1ID: std_logic_vector(7 downto 0); 
    signal BCID: std_logic_vector(11 downto 0);
    signal BCID_counter: std_logic_vector(31 downto 0); --same as BCID but does not reset on BCR and 32 bit. For debugging / timestamping in ILA.
    
    signal fastcmd_fifo_rd_en : std_logic := '0';
    signal fastcmd_fifo_wr_en, fastcmd_fifo_full, fastcmd_fifo_empty: std_logic;
    signal fastcmd_fifo_din, fastcmd_fifo_dout: std_logic_vector(43 downto 0);
    
    signal Kchar_HGTD_IDLE: std_logic_vector(7 downto 0);
    signal hgtd_idle_char_counter: integer range 0 to 2;
  
    signal cnt: integer range 0 to packetlength+4;
  
    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk160          : out    std_logic;
      clk320          : out    std_logic;
      clk640          : out    std_logic;
      clk32          : out    std_logic;
      clk64          : out    std_logic;
      clk128          : out    std_logic;
      clk40          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1_p         : in     std_logic;
      clk_in1_n         : in     std_logic
     );
    end component;
    
    COMPONENT vio_fastcmd
      PORT (
        clk : IN STD_LOGIC;
        probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in6 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        probe_in7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
      );
    END COMPONENT;
    
    COMPONENT ila_8b10b_out
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT;
    
    COMPONENT l1id_bcid_ila
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;
begin

clk200_buf: IBUFDS
port map(
    I => REFCLK_P,
    IB => REFCLK_N,
    O => clk200_ibuf
);

clk200_bufg: BUFG
port map(
    I => clk200_ibuf,
    O => clk200
);

clk0 : clk_wiz_0
   port map ( 
  -- Clock out ports  
   clk160 => clk160,
   clk320 => clk320,
   clk640 => clk640,
   clk32 => clk32,
   clk64 => clk64,
   clk128 => clk128,
   clk40 => clk40,
   reset => clk_wiz_reset,
   locked => locked,
   clk_in1_p => LPGBT_CLK40M_P,
   clk_in1_n => LPGBT_CLK40M_N
 );
 
g_datarate_320: if DATARATE = 320 generate
    serialclk <= clk160;
    dataclk <= clk32;
end generate;

g_datarate_640: if DATARATE = 640 generate
    serialclk <= clk320;
    dataclk <= clk64;
end generate;

g_datarate_1280: if DATARATE = 1280 generate
    serialclk <= clk640;
    dataclk <= clk128;
end generate;

    clk_wiz_reset <= not LPGBT_HARD_RSTB;
    reset <= not locked;
 
    
     serT0: entity work.oserdes_10b
      Port map(
      din => data_10b,
      serial_out_p => TIMING_DOUT_P(0),
      serial_out_n => TIMING_DOUT_N(0),
      reset  => reset,
      --tready => tready,
      dataclk => dataclk,
      serialclk => serialclk);

     serT1: entity work.oserdes_10b
      Port map(
      din => data_10b_inv,
      serial_out_p => TIMING_DOUT_N(1),
      serial_out_n => TIMING_DOUT_P(1),
      reset  => reset,
      --tready => open,
      dataclk => dataclk,
      serialclk => serialclk);
    
    
     serL0: entity work.oserdes_10b
      Port map(
      din => data_10b_inv,
      serial_out_p => LUMI_DOUT_N(0),
      serial_out_n => LUMI_DOUT_P(0),
      reset  => reset,
      --tready => open,
      dataclk => dataclk,
      serialclk => serialclk);

     serL1: entity work.oserdes_10b
      Port map(
      din => data_10b,
      serial_out_p => LUMI_DOUT_P(1),
      serial_out_n => LUMI_DOUT_N(1),
      reset  => reset,
      --tready => open,
      dataclk => dataclk,
      serialclk => serialclk);
   
    
datagen: process(dataclk)
--    variable cnt: integer range 0 to packetlength+4;
begin
    if rising_edge(dataclk) then
        if reset = '1' then
            cnt <= packetlength;
            data_8b <= K_28_5;
            CharIsK <= '1';
            Kchar_HGTD_IDLE <= K_28_5;
        else
            CharIsK <= '0';
            fastcmd_fifo_rd_en <= '0'; --default: don't read the FIFO all the time.
            
            if cnt = 0 then
                data_8b <= fastcmd_fifo_dout(7 downto 0);
            elsif cnt = 1 then
                data_8b <= fastcmd_fifo_dout(15 downto 8);
            elsif cnt = 2 then
                data_8b <= fastcmd_fifo_dout(23 downto 16);
            elsif cnt = 3 then
                data_8b <= fastcmd_fifo_dout(31 downto 24);
            elsif cnt = 4 then
                data_8b <= fastcmd_fifo_dout(39 downto 32);
            elsif cnt = 5 then
                data_8b <= x"0" & fastcmd_fifo_dout(43 downto 40);
            elsif cnt /= (packetlength) then
                data_8b <= std_logic_vector(to_unsigned(cnt,8));
                Kchar_HGTD_IDLE <= K_28_5;
                hgtd_idle_char_counter <= 1;
            else
                data_8b <=  Kchar_HGTD_IDLE;
                CharIsK <= '1';
                
                --hgtd uses a sequence of k28.5 k28.7 k28.7. We reset in the previous if statement to K28.5, after that we keep cycling between the 3.
                if hgtd_idle_char_counter = 0 then
                    Kchar_HGTD_IDLE <= K_28_5;
                else
                    Kchar_HGTD_IDLE <= K_28_7;
                end if;
                if hgtd_idle_char_counter = 2 then
                    hgtd_idle_char_counter <= 0;
                else
                    hgtd_idle_char_counter <= hgtd_idle_char_counter+1;
                end if;
            end if;
            if cnt /= packetlength then
                cnt <= cnt + 1;
            else
                if fastcmd_fifo_empty = '0' and hgtd_idle_char_counter = 0 then
                    fastcmd_fifo_rd_en <= '1'; --Read trigger information from FIFO.
                    cnt <= 0;
                end if;
            end if;
        end if;
    end if;
end process;

ila0 : ila_8b10b_out
PORT MAP (
	clk => dataclk,
	probe0 => data_8b,
	probe1(0) => CharIsK
);



enc0: entity work.enc_8b10b      
    port map( 
        reset => reset,
        clk => dataclk,
        ena => '1',
        KI => CharIsK, 
        datain => data_8b,
        dataout => data_10b
        );

data_10b_inv <= not data_10b;

vio_fastcmd0: vio_fastcmd
  PORT MAP (
    clk => clk160,
    probe_in0(0) => trigger,
    probe_in1(0) => bcr,
    probe_in2(0) => cal,
    probe_in3(0) => gbrst,
    probe_in4(0) => settrigid,
    probe_in5(0) => synclumi,
    probe_in6 => trigid,
    probe_in7(0) => fastcmd_locked
  );


fastcmd_dec0: entity work.fastcmd_decoder 
    port map(
        reset           => reset,
        clk160          => clk160,
        clk200          => clk200, 
        FAST_CMD_P      => FAST_CMD_P,
        FAST_CMD_N      => FAST_CMD_N,
        trigger_o       => trigger,
        bcr_o           => bcr,
        cal_o           => cal,
        gbrst_o         => gbrst,
        settrigid_o     => settrigid,
        synclumi_o      => synclumi,
        trigid_o        => trigid,
        locked_o        => fastcmd_locked
    );
    
    trigger_proc: process(clk160)
        variable increment_Xl1ID: std_logic;
        variable cnt4: integer range 0 to 3;
    begin
        if rising_edge(clk160) then
            if reset = '1' then
                L1ID <= (others => '0');
                XL1ID <= (others => '0');
                BCID <= x"000";
                cnt4 := 0;
                increment_Xl1ID := '0';
                BCID_counter <= x"0000_0000";
            else
                increment_Xl1ID := '0';
                if trigger = '1' then
                    if L1ID = x"FFFFFF" then
                        increment_Xl1ID := '1';
                        L1ID <= (others => '0');
                    else
                        L1ID <= L1ID + 1;
                    end if;
                    
                end if;
                if settrigid = '1' then
                    increment_Xl1ID := '1';
                    L1ID <= x"000" & trigid;
                end if;
                if increment_XL1ID = '1' then
                    XL1ID <= XL1ID + 1;
                end if;
                if cnt4 = 3 then
                    cnt4 := 0;
                    BCID <= BCID + 1;
                    BCID_counter <= BCID_counter + 1;
                else
                    cnt4 := cnt4+1;
                end if;
                if bcr = '1' then
                    BCID <= x"000";
                end if;
            end if;
        end if;
    end process;
    
    
    --FIFO to cross clock domain from clk160 to whatever the data clock is.
    fastcmd_fifo0: xpm_fifo_async
   generic map (
      CASCADE_HEIGHT => 0,
      CDC_SYNC_STAGES => 2,
      DOUT_RESET_VALUE => "0",
      ECC_MODE => "no_ecc",
      FIFO_MEMORY_TYPE => "auto",
      FIFO_READ_LATENCY => 1,
      FIFO_WRITE_DEPTH => 16,
      FULL_RESET_VALUE => 0,
      PROG_EMPTY_THRESH => 6,
      PROG_FULL_THRESH => 10,
      RD_DATA_COUNT_WIDTH => 1,
      READ_DATA_WIDTH => 44,
      READ_MODE => "std",
      RELATED_CLOCKS => 0,
      SIM_ASSERT_CHK => 0,
      USE_ADV_FEATURES => "0000",
      WAKEUP_TIME => 0,
      WRITE_DATA_WIDTH => 44,
      WR_DATA_COUNT_WIDTH => 1
   )
   port map (
      almost_empty => open,
      almost_full => open,
      data_valid => open, 
      dbiterr => open,
      dout => fastcmd_fifo_dout,
      empty => fastcmd_fifo_empty,
      full => fastcmd_fifo_full,
      overflow => open,
      prog_empty => open,
      prog_full => open,
      rd_data_count => open,
      rd_rst_busy => open,
      sbiterr => open,
      underflow => open,
      wr_ack => open,
      wr_data_count => open,
      wr_rst_busy => open,
      din => fastcmd_fifo_din,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rd_clk => dataclk,
      rd_en => fastcmd_fifo_rd_en,
      rst => reset,
      sleep => '0',
      wr_clk => clk160,
      wr_en => fastcmd_fifo_wr_en
   );   
   
    fastcmd_fifo_din <= BCID & XL1ID & L1ID;
   
    l1id_bcid_ila0: l1id_bcid_ila
    PORT MAP (
        clk => clk160,
        probe0 => BCID_counter, 
        probe1 => L1ID,
        probe2(0) => trigger
    );

   
   fastcmd_fifo_wr_en <= trigger and not fastcmd_fifo_full;
   
   

end architecture rtl;
